module DearImgui.Bindings.Define
  ( Define(..)
  ) where

import DearImgui.Bindings.Generic

import Data.Text (Text)
import Data.Vector (Vector)

import DearImgui.Bindings.Comments (Comments)
import DearImgui.Bindings.Conditional (Conditional)

data Define = Define
  { name         :: Text
  , content      :: Text
  , comments     :: Maybe Comments
  , conditionals :: Maybe (Vector Conditional)
  }
  deriving stock (Eq, Show, Generic)

instance FromJSON Define where
  parseJSON = genericParseJSON

instance ToJSON Define where
  toJSON = genericToJSON
