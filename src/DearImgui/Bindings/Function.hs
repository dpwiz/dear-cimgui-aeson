module DearImgui.Bindings.Function
  ( Function(..)
  ) where

import DearImgui.Bindings.Generic

import Data.Text (Text)
import Data.Vector (Vector)

import DearImgui.Bindings.Type (Argument, Type)
import DearImgui.Bindings.Comments (Comments)
import DearImgui.Bindings.Conditional (Conditional)

data Function = Function
  { name                          :: Text
  , original_fully_qualified_name :: Text
  , return_type                   :: Type
  , arguments                     :: Vector Argument
  , is_default_argument_helper    :: Bool
  , is_manual_helper              :: Bool
  , is_imstr_helper               :: Bool
  , has_imstr_helper              :: Bool
  , is_internal                   :: Maybe Bool
  , comments                      :: Maybe Comments
  , conditionals                  :: Maybe (Vector Conditional)
  }
  deriving stock (Eq, Show, Generic)

instance FromJSON Function where
  parseJSON = genericParseJSON

instance ToJSON Function where
  toJSON = genericToJSON
