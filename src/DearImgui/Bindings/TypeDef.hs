module DearImgui.Bindings.TypeDef
  ( TypeDef(..)
  ) where

import DearImgui.Bindings.Generic

import Data.Text (Text)
import Data.Vector (Vector)

import DearImgui.Bindings.Comments (Comments)
import DearImgui.Bindings.Conditional (Conditional)
import DearImgui.Bindings.Type (Type)

data TypeDef = TypeDef
  { name         :: Text
  , type_        :: Type -- ^ The defined type (as a generic type element)
  , comments     :: Maybe Comments
  , conditionals :: Maybe (Vector Conditional)
  }
  deriving stock (Eq, Show, Generic)

instance FromJSON TypeDef where
  parseJSON = genericParseJSON

instance ToJSON TypeDef where
  toJSON = genericToJSON
