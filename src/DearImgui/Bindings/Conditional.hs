module DearImgui.Bindings.Conditional
  ( Conditional(..)
  , Condition(..)
  ) where

import DearImgui.Bindings.Generic

import Data.Text (Text)
import Data.Aeson qualified as Aeson

data Conditional = Conditional
  { condition  :: Condition
  , expression :: Text
  }
  deriving stock (Eq, Show, Generic)

instance FromJSON Conditional where
  parseJSON = genericParseJSON

instance ToJSON Conditional where
  toJSON = genericToJSON

data Condition
  = Ifdef
  | Ifndef
  | If
  | Ifnot
  deriving stock (Eq, Show, Generic)

instance FromJSON Condition where
  parseJSON = Aeson.withText "Condition" \case
    "ifdef"  -> pure Ifdef
    "ifndef" -> pure Ifndef
    "if"     -> pure If
    "ifnot"  -> pure Ifnot
    unknown  -> fail $ "Unexpected condition: " <> show unknown

instance ToJSON Condition where
  toJSON = \case
    Ifdef  -> "ifdef"
    Ifndef -> "ifndef"
    If     -> "if"
    Ifnot  -> "ifnot"
