module DearImgui.Bindings.Metadata
  ( Metadata(..)
  ) where

import Prelude hiding (Enum)
import DearImgui.Bindings.Generic

import Data.Vector (Vector)

import DearImgui.Bindings.Define (Define)
import DearImgui.Bindings.Enum (Enum)
import DearImgui.Bindings.Function (Function)
import DearImgui.Bindings.Struct (Struct)
import DearImgui.Bindings.TypeDef (TypeDef)

data Metadata = Metadata
  { defines   :: Vector Define
  , enums     :: Vector Enum
  , typedefs  :: Vector TypeDef
  , structs   :: Vector Struct
  , functions :: Vector Function
  }
  deriving stock (Eq, Show, Generic)

instance FromJSON Metadata where
  parseJSON = genericParseJSON

instance ToJSON Metadata where
  toJSON = genericToJSON
