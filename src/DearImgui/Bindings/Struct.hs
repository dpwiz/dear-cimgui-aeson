module DearImgui.Bindings.Struct
  ( Struct(..)
  , Field(..)
  , FieldName(..)
  ) where

import DearImgui.Bindings.Generic

import Data.Text (Text)
import Data.Vector (Vector)

import DearImgui.Bindings.Comments (Comments)
import DearImgui.Bindings.Conditional (Conditional)
import DearImgui.Bindings.Type (Type, RawArrayBounds)

data Struct = Struct
  { name                          :: Text
  , original_fully_qualified_name :: Text
  , type_                         :: Text -- StructType
  , by_value                      :: Bool
  , forward_declaration           :: Bool
  , is_anonymous                  :: Bool
  , fields                        :: Vector Field
  , is_internal                   :: Maybe Bool
  , comments                      :: Maybe Comments
  , conditionals                  :: Maybe (Vector Conditional)
  }
  deriving stock (Eq, Show, Generic)

instance FromJSON Struct where
  parseJSON = genericParseJSON

instance ToJSON Struct where
  toJSON = genericToJSON

data Field = Field
  { names        :: Vector FieldName
  , is_anonymous :: Bool
  , type_        :: Type
  , is_internal  :: Maybe Bool
  , comments     :: Maybe Comments
  , conditionals :: Maybe (Vector Conditional)
  }
  deriving stock (Eq, Show, Generic)

instance FromJSON Field where
  parseJSON = genericParseJSON

instance ToJSON Field where
  toJSON = genericToJSON

data FieldName = FieldName
  { name         :: Text
  , is_array     :: Bool
  , array_bounds :: Maybe RawArrayBounds
  , width        :: Maybe Text
  }
  deriving stock (Eq, Show, Generic)

instance FromJSON FieldName where
  parseJSON = genericParseJSON

instance ToJSON FieldName where
  toJSON = genericToJSON
