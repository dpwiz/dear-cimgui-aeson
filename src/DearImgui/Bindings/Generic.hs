module DearImgui.Bindings.Generic
  ( Generic(..)
  , Aeson.ToJSON(..)
  , Aeson.FromJSON(..)
  , genericParseJSON
  , genericToJSON
  , strictOptions
  ) where

import Data.Aeson (Value, Zero, GFromJSON, GToJSON')
import Data.Aeson qualified as Aeson
import Data.Aeson.Types (Parser)
import GHC.Generics (Generic(..))

genericParseJSON :: (Generic a, GFromJSON Zero (Rep a)) => Value -> Parser a
genericParseJSON = Aeson.genericParseJSON strictOptions

genericToJSON :: (Generic a, GToJSON' Value Zero (Rep a)) => a -> Value
genericToJSON = Aeson.genericToJSON strictOptions

strictOptions :: Aeson.Options
strictOptions = Aeson.defaultOptions
  { Aeson.rejectUnknownFields =
      True
  , Aeson.omitNothingFields =
      True
  , Aeson.fieldLabelModifier = \case
      "type_" -> "type"
      ok      -> ok
  }
