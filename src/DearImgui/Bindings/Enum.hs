module DearImgui.Bindings.Enum
  ( Enum(..)
  , Element(..)
  ) where

import Prelude hiding (Enum)
import DearImgui.Bindings.Generic

import Data.Text (Text)
import Data.Vector (Vector)

import DearImgui.Bindings.Comments (Comments)
import DearImgui.Bindings.Conditional (Conditional)
import DearImgui.Bindings.Type (Type)

data Enum = Enum
  { name                          :: Text
  , original_fully_qualified_name :: Text
  , storage_type                  :: Maybe Type
  , elements                      :: Vector Element
  , comments                      :: Maybe Comments
  , conditionals                  :: Maybe (Vector Conditional)
  }
  deriving stock (Eq, Show, Generic)

instance FromJSON Enum where
  parseJSON = genericParseJSON

instance ToJSON Enum where
  toJSON = genericToJSON

data Element = Element
  { name         :: Text
  , value        :: Maybe RawValue
  , comments     :: Maybe Comments
  , conditionals :: Maybe (Vector Conditional)
  , is_internal  :: Maybe Bool
  }
  deriving stock (Eq, Show, Generic)

instance FromJSON Element where
  parseJSON = genericParseJSON

instance ToJSON Element where
  toJSON = genericToJSON

type RawValue = Text
