module DearImgui.Bindings.Type
  ( Type(..)
  , Details(..)
  , Argument(..)
  , RawArrayBounds
  , RawDefaultValue
  ) where

import DearImgui.Bindings.Generic

import Data.Text (Text)
import Data.Vector (Vector)

data Type = Type
  { declaration :: Text
  , type_details :: Maybe Details
  }
  deriving stock (Eq, Show, Generic)

instance FromJSON Type where
  parseJSON = genericParseJSON

instance ToJSON Type where
  toJSON = genericToJSON

data Details = Details
  { flavour :: Text
  , return_type :: Maybe Type
  , arguments :: Maybe (Vector Argument)
  }
  deriving stock (Eq, Show, Generic)

instance FromJSON Details where
  parseJSON = genericParseJSON

instance ToJSON Details where
  toJSON = genericToJSON

data Argument = Argument
  { name          :: Text
  , type_         :: Maybe Type
  , is_array      :: Bool
  , array_bounds  :: Maybe RawArrayBounds
  , is_varargs    :: Bool
  , default_value :: Maybe RawDefaultValue
  }
  deriving stock (Eq, Show, Generic)

instance FromJSON Argument where
  parseJSON = genericParseJSON

instance ToJSON Argument where
  toJSON = genericToJSON

type RawDefaultValue = Text

type RawArrayBounds = Text
