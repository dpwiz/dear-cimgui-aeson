module DearImgui.Bindings.Comments
  ( Comments(..)
  ) where

import DearImgui.Bindings.Generic

import Data.Text (Text)
import Data.Vector (Vector)

data Comments = Comments
  { preceding :: Maybe (Vector Text)
  , attached :: Maybe Text
  }
  deriving stock (Eq, Show, Generic)

instance FromJSON Comments where
  parseJSON = genericParseJSON

instance ToJSON Comments where
  toJSON = genericToJSON
