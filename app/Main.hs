{-# OPTIONS_GHC -fsimpl-tick-factor=1000 #-}

module Main (main) where

import GHC.SourceGen

import Control.Monad
import Data.Either
import Data.List qualified as List
import Data.Map.Strict (Map)
import Data.Map.Strict qualified as Map
import Data.Ord (comparing)
import Data.Text (Text)
import Data.Text qualified as Text
import Data.Vector (Vector)
import Data.Vector qualified as Vector
import Data.Vector.Algorithms.Tim qualified as Tim
import Debug.Trace
import GHC (runGhc)
import GHC.Paths (libdir)
import GHC.Records (HasField(..))
import System.IO (stdout)

import DearImgui.Bindings qualified as Bindings
import DearImgui.Metadata (metadata)

main :: IO ()
main = do
  forM_ structs \struct -> do
    print struct.name
    Vector.forM_ struct.fields \field ->
      print $ fmap ((, field.type_.declaration) . (.name)) field.names
      -- putStrLn $ "    " <> Text.unpack (field.name <> " :: " <> field.type_.declaration)

  -- forM_ notMethods \fun ->
    -- print (fun.original_fully_qualified_name, fun.name)

  -- forM_ (Map.toList objects) \(self, methods) -> do
  --   traceShowM self
  --   forM_ (List.sortBy (comparing fst) methods) \(method, fun) ->
  --     putStrLn $ "  " <> show (method, fun.original_fully_qualified_name, method == fun.original_fully_qualified_name)

  -- runGhc (Just libdir) $
    -- hPutPpr stdout imguiModule

-- | Free-standing structs, like ImVec4
structs :: Map Text Bindings.Struct
structs = Map.filterWithKey
  ( \name struct ->
      Map.notMember name objects &&
      not (internal struct) &&
      struct.fields /= mempty
  )
  structs'

-- | Opaque pointers for objects with methods
objects :: Map Text [(Text, Bindings.Function)]
objects = Map.fromListWith mappend methods'

notMethods :: [Bindings.Function]
methods' :: [(Text, [(Text, Bindings.Function)])]
(notMethods, methods') = partitionEithers do
  fun <- Vector.toList functions

  if Vector.null fun.arguments then
    pure $ Left fun
  else case Vector.unsafeHead fun.arguments of
    Bindings.Argument{name, type_=Just argType}
      | name == "self" && Text.isSuffixOf "*" argType.declaration -> do
          let
            -- XXX: Drop "const _*"
            self = last . Text.words $ Text.init argType.declaration
          case Text.stripPrefix (self <> "_") fun.name of
            Nothing ->
              fail $
                "Method name doesn't match its self struct: " <> show (self, fun.name)
            Just method -> do
              unless (Map.member self structs') $
                fail $ "struct not found for method: " <> show (self, method)
              pure $ Right
                ( self
                , [(method, fun)]
                )
      -- []
    _ ->
      pure $ Left fun

structs' :: Map Text Bindings.Struct
structs' =
  Map.fromListWithKey oops do
    struct <- Vector.toList $
      Vector.filter (not . obsolete) metadata.structs
    pure (struct.name, struct)
  where
    oops k _ _ =
      error $ mappend "duplicate Struct key: " (show k)

functions :: Vector Bindings.Function
functions =
  Vector.modify (Tim.sortBy $ comparing (.name)) $
    Vector.filter (not . obsolete) metadata.functions

enums' :: Map Text Bindings.Enum
enums' =
  Map.fromListWithKey oops do
    enum <- Vector.toList $
      Vector.filter (not . obsolete) metadata.enums
    pure (enum.name, enum)
  where
    oops k _ _ =
      error $ mappend "duplicate Struct key: " (show k)

internal :: HasField "is_internal" a (Maybe Bool) => a -> Bool
internal x = x.is_internal == Just True

obsolete :: HasField "conditionals" a (Maybe (Vector Bindings.Conditional)) => a -> Bool
obsolete x =
  case x.conditionals of
    Just conds ->
      Vector.any (\cond -> cond.expression == "IMGUI_DISABLE_OBSOLETE_FUNCTIONS") conds
    Nothing ->
      False

imguiModule :: HsModule'
imguiModule =
  module' (Just "ImGui") (Just [var "createContext"]) [] do
    mzero
    -- traceShowM (nameParts, fun.original_fully_qualified_name)
    -- []
    -- pure $ ffiDecl fun

-- ffiDecl :: Function -> HsDecl'
-- ffiDecl function = foreignImport foreignNames localName localType
--   where
--     foreignNames =
--       capiFunction "cimgui.h" (Text.unpack function.name)

--     localName =
--       "createContext"

--     fqdn = Text.splitOn "::" function.original_fully_qualified_name

--     localType =
--       ptr (var "This") -->
--       io (ptr (var "That"))

ptr :: (App e, Var e) => e -> e
ptr a = var "Ptr" @@ a

io :: (App e, Var e) => e -> e
io a = var "IO" @@ a
