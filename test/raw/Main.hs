{-# LANGUAGE CApiFFI #-}

module Main (main) where

import Foreign

data ImFontAtlas
data ImGuiContext

foreign import capi "cimgui.h ImGui_CreateContext"
  createContext
    :: Ptr ImFontAtlas
    -> IO (Ptr ImGuiContext)

main :: IO ()
main = do
  createContext nullPtr >>= print
