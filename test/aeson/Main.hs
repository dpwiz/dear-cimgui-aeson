module Main (main) where

import Data.Aeson (eitherDecodeFileStrict')
import Shower (printer)

import DearImgui.Bindings.Metadata (Metadata)

main :: IO ()
main = do
  eitherDecodeFileStrict' "generated/cimgui.json" >>= \case
    Left err ->
      fail err
    Right (res :: Metadata) ->
      -- printer res
      pure ()
